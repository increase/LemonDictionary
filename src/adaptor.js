/*!
// lemonDictionary v1.0.0
// Copyright 2015 OrChange Inc. All rights reserved.
// Licensed under the GPL License.*/

/*jslint nomen: true, vars: true */
/*global angular, Dictionary */
(function (root, factory) {
	'use strict';

	angular.module('lemonDictionary', []).provider('LD', function () {
		this.$get = [function () {
			return factory(angular);
		}];
	});
}(this, function (_) {
	'use strict';
	//= include ./dictionary.js
	//= include ./randexp.js

	return Dictionary;
}));
