/**
 * Created by Weilin Shi on 2015/8/31.
 */
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var include = require('gulp-include');

gulp.task('default', function(){
	return gulp.src('./src/adaptor.js')
		.pipe(include())
		.pipe(concat('lemonDictionary.js'))
		.pipe(gulp.dest('./dist'))
		.pipe(concat('lemonDictionary.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./dist'));
});
